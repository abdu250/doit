//
//  TasksViewController.swift
//  DoIt
//
//  Created by Abdullah Al-Ahmadi on 1/28/17.
//  Copyright © 2017 Abdullah Al-Ahmadi. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  var tasks: [Task] = []
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    tableView.dataSource = self
    tableView.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    getTasks()
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tasks.count
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    let task = tasks[indexPath.row]
    
    if task.important {
      cell.textLabel?.text = "❗️\(task.name!)"
    }else{
      cell.textLabel?.text = task.name!
    }
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    
    
    let task = tasks[indexPath.row]
    performSegue(withIdentifier: "selectedTaskSegue", sender: task)
  }
  
  
  
  @IBAction func addPressed(_ sender: Any) {
    performSegue(withIdentifier: "moveToAddTaskController", sender: nil)
  }
  
  func getTasks(){
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    do {
      tasks =  try context.fetch(Task.fetchRequest()) as! [Task]
    } catch {
      print("We Have An Error")
    }
    tableView.reloadData()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    
    if segue.identifier == "selectedTaskSegue" {
      let nextVC = segue.destination as! CompleteTaskViewController
      nextVC.task = sender as? Task
     
    }
    
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
}

